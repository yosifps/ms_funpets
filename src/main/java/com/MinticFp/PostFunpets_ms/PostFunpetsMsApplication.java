package com.MinticFp.PostFunpets_ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostFunpetsMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PostFunpetsMsApplication.class, args);
	}

}
