package com.MinticFp.PostFunpets_ms.models;
import org.springframework.data.annotation.Id;
import java.util.Date;
public class Publicaciones {
    @Id
    private String id;
    private String cuenta;
    private String texto;
    private Date date;

    public Publicaciones(String id, String cuenta, String texto, Date date) {
        this.id = id;
        this.cuenta = cuenta;
        this.texto = texto;
        this.date = date;
    }

    public String getId() {
        return id;
    }


    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
