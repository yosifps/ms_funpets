package com.MinticFp.PostFunpets_ms.repositories;
import com.MinticFp.PostFunpets_ms.models.Publicaciones;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;

public interface PublicacionesRepository extends MongoRepository <Publicaciones, String> {
    List<Publicaciones> findBycuenta (String cuenta);
}
