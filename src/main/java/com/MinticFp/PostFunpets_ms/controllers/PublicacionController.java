package com.MinticFp.PostFunpets_ms.controllers;
import com.MinticFp.PostFunpets_ms.exceptions.PostNotFoundException;
import com.MinticFp.PostFunpets_ms.models.Publicaciones;
import com.MinticFp.PostFunpets_ms.repositories.PublicacionesRepository;
import org.springframework.web.bind.annotation.*;
import java.util.List;
public class PublicacionController {
    private final PublicacionesRepository publicacionesRepository;

    public PublicacionController(PublicacionesRepository publicacionesRepository) {
        this.publicacionesRepository = publicacionesRepository;
    }
     @GetMapping("/publicaciones/{cuenta}")
     List<Publicaciones> getPublicaciones (@PathVariable String cuenta){
         List<Publicaciones> publications = publicacionesRepository.findBycuenta(cuenta);
         if (publications==null)
             throw new PostNotFoundException("No se encontro una publicación en la cuenta: " + cuenta);
     return publications;
    }
    @PostMapping("/publicaciones")
    Publicaciones newPublicación(@RequestBody Publicaciones publicacion){
        return publicacionesRepository.save(publicacion);
    }
    @GetMapping("/publicaciones/Muro")
    List<Publicaciones> Allgetpublicaciones () {
        List<Publicaciones> allpublications = publicacionesRepository.findAll();
        if (allpublications == null)
            throw new PostNotFoundException("No se encontraron publicaciones");
        return allpublications;
    }
}
